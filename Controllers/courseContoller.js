

const Course = require("../Models/coursesSchema");

const User = require('../Models/usersSchema');
const auth = require('../auth.js');

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
module.exports.addCourse = (request, response)=>{

    const userData = auth.decode(request.headers.authorization);

    console.log(userData);
    
    
    
    return User.findById(userData._id)
    .then(result => {
       if(result.isAdmin){
        let input = request.body;

        let newCourse = new Course({
            name:input.name,
            description: input.description,
            price: input.price
        })
    
        return newCourse.save()
        .then(course=> {
            console.log(course);
            response.send('couse added')
        })
        .catch(error=>{
            console.log(error);
            response.send('must be admin to add course')
        })
       }
       else{
        response.send('must be admin to add course')
       }


    })

    .catch(error=>{
        console.log(error);
        response.send('must be admin to add course')
    })
   

}


///
module.exports.allCourses = (request, response)=>{
    const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin){
        return response.send("You don't have access to this route!")
    }
    else{
        Course.find({})
        .then(result => response.send(result))
        .catch(error => response.send(error))
    }
}

// get all active courses
module.exports.allActiveCourses = (request, response)=>{
    Course.find({isActive:true})
    .then(result => response.send(result))
    .catch(error=> response.send(error))
}

//get all inactive courses
module.exports.allInActiveCourses = (request, response)=>{

    const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin){
        return response.send("You don't have access to this route!")
    }
    else{
        Course.find({isActive:false})
        .then(result => response.send(result))
        .catch(error => response.send(error))
    }
}

// 
module.exports.courseDetails = (request, response)=>{
    const courseId = request.params.courseId;

    Course.findById(courseId)
    .then(result => {
        response.send(result)
    })
    .catch(error => response.send(error))
}

// update course

module.exports.updateCourse = async (request, response)=>{
    const userData = auth.decode(request.headers.authorization);
    const input = request.body;
    const courseId = request.params.courseId;

    let updatedCourse = {
        name: input.name,
        description: input.description,
        price: input.price
    }

    if(!userData.isAdmin){
        response.send("You are not authorize to this page!")
    }
    else{
       await Course.findOne({_id: courseId})
        .then(result => {
            if(result === null){
                response.send("Course ID is invalid")
            }
            else{
                Course.findByIdAndUpdate(courseId, updatedCourse, {new:true})
                .then(result => response.send(result))
                .catch(error => response.send(error))
            }
        })
        .catch(error => response.send(error))
       
       
       
    }
}



/// archive course

module.exports.archiveCourse = (request, response)=>{
    const input = request.body;
    const courseID = request.params.courseId;

    const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin){
        response.send("You are not authorize to this page!")
    }
    else{
        Course.findById(courseID)
        .then(result => {
            if(result === null){
                response.send("Course ID is invalid!")
            }
            else{
                Course.findByIdAndUpdate(courseID, {isActive: input.isActive}, {new:true})
                .then(result => response.send(`${result._id}: ${result.name} is now updated to isActive:${result.isActive}`))
                .catch(error => response.send(error))
            }
        })
        .catch(error=> response.send(error))

       
    }
}