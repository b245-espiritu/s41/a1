const mongoose = require('mongoose')

const User = require("../Models/usersSchema");
const Course = require('../Models/coursesSchema.js')
 const bcrypt = require('bcrypt');
 const auth = require('../auth.js');


/**
    This controller will create or register a 
    user on our database
 */

    module.exports.userRegistration = (request, response)=>{
        const input = request.body;

        User.findOne({email: input.email}) // this will return null if theres no email found same with the input email
        .then(result =>{
            if(result !== null){
                return response.send('The email is already exist!');
            }
            else{
                let newUser =  new User({
                    firstName: input.firstName,
                    lastName: input.lastName,
                    email: input.email,
                    password:bcrypt.hashSync(input.password, 10),
                    mobileNo:input.mobileNo
                });

                newUser.save()
                .then(save =>response.send('you are now registered to our website!'))
                .catch(error => response.send(error))
            }
        })
        .catch( error => response.send(error))
    }




// user log in Authentication
module.exports.userAuthentication = (request, response)=>{
    let input = request.body;


    //Possible scenarios in logging in
    // 1. email is not yet registered
    // 2. email is registered but the password is wrong

    User.findOne({email: input.email})
    .then(result => {
        if(result === null){
            return response.send('Email is not yet registered. Register first before loggin in!')
        } 
        else{

            // compareSync method is used to compare a non encrypted password
            // to encrypted password.

            // it returns boolean value, if match true or false if not match
            const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

            if (isPasswordCorrect){
                return response.send({auth: auth.createAccessToken(result)})
            }
            else{
                response.send('Password is incorrect!')
            }
        }
        
    })
    .catch(error => response.send(error))
}




// get user by id display user details

module.exports.getProfile =(request, response)=>{
    // let input = request.body;

    const userData = auth.decode(request.headers.authorization);

    console.log(userData);

   

   return User.findById(userData._id)
   .then(result=>{
        result.password="";
        return response.send(result);
    })


     // User.findOne({userData._id)
    // .then( result =>{
    //     if(result !== null){
            
    //         result.password = ''
    //       return  response.send(result)
    //     }
    //     else{
    //        return response.send('not valid id')
    //     }
    // })
    // .catch(error => response.send(error))
    



    // User.findOne({_id:input._id})
    // .then(result => {
    //     if(result !==null){
    //         return response.send({...result._doc, password:''})
    //     }
    //     else{
    //         return response.send('ID not valid')
    //     }
    // })

    // .catch(error => response.send(error))

    
    // User.findByIdAndUpdate(input._id, {password:''}, {new:true})  
    // .then(result => response.send(result))
    // .catch(error => response.send(error))

}

// Controller for user enrollment

module.exports.enrollCourse = (request, response)=>{

    const userData = auth.decode(request.headers.authorization);
    const courseId = request.params.courseId;

    if(!userData.isAdmin){

        Course.findById(courseId)
            .then(result => {
                if(result === null ){
                    response.send("course ID is invalid!")
                }
                else{

                    
                        let isUserUpdated =  User.findById(userData._id)
                        .then(result => {
                            if(result === null){
                                return false
                            }
                            else{
                                result.enrollments.push({
                                    courseId: courseId
                                })

                                return result.save()
                                .then(save => true)
                                .catch(error=> false)
                            }
                        })



                        let isCourseUpdated =  Course.findById(courseId)
                        .then(result => {
                            if(result === null ){
                                return false;
                            }
                            else{
                                result.enrolles.push(
                                    {
                                        userId:userData._id
                                    }
                                )
                                
                                return result.save()
                                .then(save => true)
                                .catch(error => false)
                            }
                        })


                        if(isUserUpdated && isCourseUpdated){
                            return response.send('You are now enrolled to the course!')
                        }
                        else{
                            response.send('there was an error during enrollment. Please try again!')
                        }


                }
            })
            .catch(error => response.send(error))



    }
    else{
        response.send("don't have access to enrollement")
        }      

}
